import pandas as pd
from sklearn.model_selection import train_test_split
from typing import List

class TrainingData:
    def __init__(self, dataset_path: str, to_drop: List[str], target: str): 
        X = pd.read_csv(dataset_path)
        X.drop(to_drop, axis=1, inplace=True)
        y = X.pop(target)

        self.x_train, self.x_test, self.y_train, self.y_test = train_test_split(
            X,
            y,
            test_size=0.2,
            stratify= y,
            random_state=0
        )