import joblib
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, confusion_matrix

from src.constants import AGGREGATOR_MODEL_PATH, CM_PLOT_PATH
from src.models.aggregator_model import AggregatorModel
from src.models.decision_tree_model import DecisionTreeModel
from src.models.random_forest_model import RandomForestModel
from src.models.KNeighbors_model import KNeighborsModel
from src.models.Logistic_regression_model import LogisticRegressionModel
from src.models.XGBoost_model import XGBoostModel

from src.models.svc_model import SVCModel
from src.utils import PlotUtils
from src.training.train_data import TrainingData
from typing import List
from sklearn.inspection import permutation_importance 


models = {'Decision Tree': DecisionTreeModel,
          'Random Forest': RandomForestModel, 
          'SVC': SVCModel,
          'Logistic Regression': LogisticRegressionModel,
          'XGBoost': XGBoostModel,
          'KNeighbors': KNeighborsModel
          }

class TrainingPipeline:
    def __init__(self, data: TrainingData, models_names: List[str]):
        self.data = data
        self.agg_model = AggregatorModel(
            [models[models_name]() for models_name in models_names]
            )
        self.file_name = "_".join(sorted(models_names))

    def train(self, serialize: bool = True):
        model_path = str(AGGREGATOR_MODEL_PATH).replace(
            'aggregator_model.joblib', self.file_name + '.joblib')
        try:
            self.agg_model = joblib.load(model_path)
        except FileNotFoundError:
            self.agg_model.fit(
                self.data.x_train,
                self.data.y_train
            )

            if serialize:
                self.agg_model.save(model_path)
    
    def get_predictions(self):
        return self.agg_model.predict(self.data.x_test)

    def get_model_perfomance(self, ) -> dict:
        predictions = self.get_predictions()
        return {'Accuracy': accuracy_score(self.data.y_test, predictions),
            'Recall': recall_score(self.data.y_test, predictions),
            'Precision': precision_score(self.data.y_test, predictions),
            'F1_score': f1_score(self.data.y_test, predictions),
            # 'AUC_score':accuracy_score(self.data.y_test, predictions)
        }
    def get_permutation_importance(self):
        return permutation_importance(self.agg_model, self.data.x_test, self.data.y_test, n_repeats=10, scoring= 'f1',
                                        random_state=42, n_jobs=2)
        

    def render_evaluation_plots(self):
        predictions = self.get_predictions()
        perf_dict = self.get_model_perfomance()
        cm = confusion_matrix(self.data.y_test, predictions, labels=[0, 1])
        fig, axs = plt.subplots(1,2,figsize=(10,5))
        PlotUtils.plot_confusion_matrix(
            cm,
            classes=['Clear(0)', 'Fraudulent(1)'],
            normalize=False,
            title='Current Model',
            ax=axs[0]
        )
        PlotUtils.plot_model_perf(
            perf_dict,
            title='Current Model',
            ax= axs[1]
        )
        plot_path = str(CM_PLOT_PATH).replace(
            'cm_plot.png', self.file_name + 'eval_plots.png')
        fig.savefig(plot_path)
        return fig, plot_path
        # plt.show()

    def render_permutation_importance(self ):
        permutation_result = self.get_permutation_importance()
        fig, axs = plt.subplots(1,1, figsize=(10,5))
        PlotUtils.plot_permutation_importance(permutation_result, self.data.x_test, axs,  palette = 'YlGnBu')
        plot_path = str(CM_PLOT_PATH).replace(
            'cm_plot.png', self.file_name + 'perm_plot.png')
        fig.savefig(plot_path)
        return fig, plot_path


if __name__ == "__main__":
    tp = TrainingPipeline()
    tp.train(serialize=True)
    accuracy, f1 = tp.get_model_perfomance()
    tp.render_confusion_matrix()
    print("ACCURACY = {}, F1 = {}".format(accuracy, f1))
