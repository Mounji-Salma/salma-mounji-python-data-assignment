import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style("whitegrid")
import pandas as pd
from math import ceil


class PlotUtils:

    @staticmethod
    def plot_confusion_matrix(cm, classes, title, ax, normalize=False, cmap="YlGnBu") :  

        title = 'Confusion Matrix of {}'.format(title)
        if normalize:
            cm = cm.astype(float) / cm.sum(axis=1)[:, np.newaxis]

        tick_marks = np.arange(len(classes))
        ax.set_xticks(tick_marks, classes)
        ax.set_yticks(tick_marks, classes)
        # create heatmap
        sns.heatmap(pd.DataFrame(cm), annot=True, cmap="YlGnBu" ,fmt='g', ax=ax)
        ax.xaxis.set_label_position("top")
        ax.set_title(title)
        ax.set_ylabel('Actual label')
        ax.set_xlabel('Predicted label')

    @staticmethod
    def plot_model_perf(metrics_dict : dict, title, ax, palette='YlGnBu'):
        perf=pd.Series(metrics_dict)
        p=sns.barplot(x=perf.index, y=perf.values,ax=ax, palette= palette)
        title= "Evaluation of "+title
        p.set_title(title)

    @staticmethod
    def plot_permutation_importance(perm_imp_res, X_test, ax, title='Permutation importance',  palette = 'YlGnBu'):
        arr = perm_imp_res.importances_mean
        arr = arr[arr != 0]
        sorted_idx = np.argsort(np.abs(arr))
        sorted_importances = np.sort(arr[sorted_idx])
        sns.barplot(x=sorted_importances, y= X_test.columns[sorted_idx],palette = palette, ax=ax)
        ax.set_title(title, fontsize=16)
        ax.set_xlabel('feature importance',  fontsize=14)    
  
    @staticmethod
    def donut_plot(df, col, ax, title, label_names=None, text='', colors=['#E9967A', 'cadetblue'], circle_radius=0.8,
             flag_ruido=0):
        def make_autopct(values):
            def my_autopct(pct):
                total = sum(values)
                val = int(round(pct * total / 100.0))

                return '{p:.1f}%\n({v:d})'.format(p=pct, v=val)
            return my_autopct
        text= f'Total\n{len(df)}'
        values = df[col].value_counts().values
        if label_names is None:
            label_names = df[col].value_counts().index

        if flag_ruido > 0:
            values = values[:-flag_ruido]
            label_names = label_names[:-flag_ruido]

        center_circle = plt.Circle((0, 0), circle_radius, color='white')
        ax.pie(values, labels=label_names, colors=colors, autopct=make_autopct(values))
        ax.add_artist(center_circle)

        kwargs = dict(size=20, fontweight='bold', va='center')
        ax.text(0, 0, text, ha='center', **kwargs)
        ax.set_title(title, size=14, color='dimgrey')
        
    # def distribution_plot(column, title, ax, color):
    #     sns.distplot(column, ax=ax, color=color)
    #     ax.set_title('Distribution of '+title, fontsize=14)
    #     ax.set_xlim([min(column), max(column)])

    def distribution_plots(df, hue):
        fig, axs = plt.subplots(ceil((df.shape[1]-1)/3), 3, figsize=(10, 20))
        axs = axs.flatten()
        for i, col in enumerate(df.columns[df.columns != hue]):
            sns.distplot(df[col][df[hue] == 1], bins=50, ax=axs[i], color= '#4682B4')
            sns.distplot(df[col][df[hue] == 0], bins=50, ax=axs[i], color= 'salmon')
            axs[i].set_xlabel('')
            axs[i].set_title('Distribution of feature: ' + str(col))
        fig.tight_layout()
        return fig   

    def interaction_plot(df, col):
        sns.pairplot(df, hue=col, plot_kws=dict(alpha=0.5), corner=True,palette=['rosybrown', 'steelblue'])
# %%
for i, elt in enumerate([1,2,3]):
    print(i)
    print(elt)