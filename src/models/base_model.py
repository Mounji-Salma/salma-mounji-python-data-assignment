import numpy as np
import joblib

from src.models.estimator_interface import EstimatorInterface


class BaseModel(EstimatorInterface):
    def __init__(self, model: object = None):
        self.model = model

    def fit(self, x_train: np.ndarray, y_train: np.ndarray) -> object:
        return self.model.fit(x_train, y_train)

    def predict(self, x_test: np.ndarray) -> np.ndarray:
        return self.model.predict(x_test)

    # model = BaseModel.load("my_path")
    @staticmethod
    def load(model_path: str):
        return joblib.load(model_path)

    # model.save("my_path")
    def save(self, path: str = 'model.joblib'):
        joblib.dump(self, path)
