from sklearn.tree import DecisionTreeClassifier

from src.models.base_model import BaseModel

# remove attributes and add keywords arguments to support all DecisionTreeClassifier arguments
class DecisionTreeModel(BaseModel):
    def __init__(self, max_depth: int = 4, criterion: str = 'entropy', **kwargs):
        super().__init__(
            model=DecisionTreeClassifier(
                max_depth=max_depth,
                criterion=criterion,
                **kwargs
            )
        )

