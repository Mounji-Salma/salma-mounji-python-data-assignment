from sklearn.linear_model import LogisticRegression

from src.models.base_model import BaseModel

class LogisticRegressionModel(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(
            model=LogisticRegression(
                **kwargs
            )
        )