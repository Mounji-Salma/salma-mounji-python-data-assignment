import numpy as np
from typing import Optional

from src.models.base_model import BaseModel


class AggregatorModel(BaseModel):
    def __init__(self, models: Optional[list] = None):
        self.models = models

    def fit(self, x_train: np.ndarray, y_train: np.ndarray) -> object:
        return [
            model.fit(x_train, y_train) for model in self.models
        ]

    def predict(self, x_test: np.ndarray) -> np.ndarray:
        return np.mean([model.predict(x_test) for model in self.models],
                       axis=0).round().astype(int)