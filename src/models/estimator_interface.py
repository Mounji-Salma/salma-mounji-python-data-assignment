import pandas as pd


class EstimatorInterface:
    def fit(self, x_train: pd.DataFrame, y_train: pd.DataFrame) -> object:
        pass

    def predict(self, x_test: pd.DataFrame) -> pd.DataFrame:
        pass

    @classmethod
    def save(cls, path: str = 'model.joblib'):
        pass

    def load(self,model_path: str) -> object:
        pass
