from xgboost import XGBClassifier

from src.models.base_model import BaseModel

class XGBoostModel(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(
            model=XGBClassifier(
                **kwargs
            )
        )