from sklearn.ensemble import RandomForestClassifier

from src.models.base_model import BaseModel

# remove attributes and add keywords arguments to support all RandomForestClassifier arguments
class RandomForestModel(BaseModel):
    def __init__(self, max_depth: int = 4, criterion: str = 'entropy', **kwargs):
        super().__init__(
            model=RandomForestClassifier(
                max_depth=max_depth,
                criterion=criterion,
                **kwargs
            )
        )