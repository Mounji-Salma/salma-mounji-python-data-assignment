# ./run.sh

import streamlit as st
import pandas as pd
import matplotlib.pyplot as plt
import requests


from src.constants import INFERENCE_EXAMPLE, DATASET_PATH, CM_PLOT_PATH
from src.training.train_pipeline import TrainingPipeline, models
from src.training.train_data import TrainingData
from src.utils import PlotUtils



st.title("Card Fraud Detection Dashboard")
st.sidebar.title("Data Themes")

sidebar_options = st.sidebar.selectbox(
    "Options",
    ("EDA", "Training", "Inference")
)

if sidebar_options == "EDA":
    dataset = pd.read_csv(DATASET_PATH)
    st.header("Exploratory Data Analysis") 
    st.write('Features overview')
    st.dataframe(dataset.describe())
    st.write('Target distribution')
    col1, col2 = st.columns(2)
    fig, axs = plt.subplots(1,1, figsize=(5,5))
    PlotUtils.donut_plot(dataset, 'Class', label_names= ['No Fraud', 'Fraud'], ax= axs, title= "Target distribution")
    col1.pyplot(fig)
    st.write('Features distributions')
    fig2 = PlotUtils.distribution_plots(dataset, 'Class')
    st.pyplot(fig2)

elif sidebar_options == "Training":
    st.header("Model Training")
  
    models_names = st.multiselect('Select Model', options=models.keys(),
        default=['Decision Tree'])
    serialize = st.checkbox('Save model')
    train = st.button('Train Model')

    if train:
        with st.spinner('Training model, please wait...'):
            try:
                fraud_data = TrainingData(DATASET_PATH, ['Time'], 'Class')
                tp = TrainingPipeline(data=fraud_data, models_names=models_names)
                tp.train(serialize=serialize)

                evaluation_plots_fig, evaluation_plots_path = tp.render_evaluation_plots()
                accuracy, f1 = tp.get_model_perfomance()['Accuracy'], tp.get_model_perfomance()['F1_score']
                permutation_importance_fig, permutation_importance_path = tp.render_permutation_importance()
                col1, col2 = st.columns(2)
                col1.metric(label="Accuracy score",
                            value=str(round(accuracy, 4)))
                col2.metric(label="F1 score", value=str(round(f1, 4)))
                st.pyplot(evaluation_plots_fig)
                st.pyplot(permutation_importance_fig)

            except Exception as e:
                st.error('Failed to train model!')
                st.exception(e)


else:
    st.header("Fraud Inference")
    st.info("This section simplifies the inference process. "
            "You can tweak the values of feature 1, 2, 19, "
            "and the transaction amount and observe how your model reacts to these changes.")
    feature_11 = st.slider('Transaction Feature 11', -
                           10.0, 10.0, step=0.001, value=-4.075)
    feature_13 = st.slider('Transaction Feature 13', -
                           10.0, 10.0, step=0.001, value=0.963)
    feature_15 = st.slider('Transaction Feature 15', -
                           10.0, 10.0, step=0.001, value=2.630)
    amount = st.number_input(
        'Transaction Amount', value=1000, min_value=0, max_value=int(1e10), step=100)
    infer = st.button('Run Fraud Inference')

    INFERENCE_EXAMPLE[11] = feature_11
    INFERENCE_EXAMPLE[13] = feature_13
    INFERENCE_EXAMPLE[15] = feature_15
    INFERENCE_EXAMPLE[28] = amount

    if infer:
        with st.spinner('Running inference...'):
            try:
                result = requests.post(
                    'http://localhost:3333/api/inference',
                    json=INFERENCE_EXAMPLE
                )
                if int(int(result.text) == 1):
                    st.success('Done!')
                    st.metric(label="Status", value="Transaction: Fraudulent")
                else:
                    st.success('Done!')
                    st.metric(label="Status", value="Transaction: Clear")
            except Exception as e:
                st.error('Failed to call Inference API!')
                st.exception(e)
